---
weight: 5
title: "Cara cepat cek size node_modules di Linux/Mac"
date: 2022-03-22T16:20:03Z
lastmod: 2022-03-22T16:20:03Z
draft: false
author: "Suryo Prasetyo W"
authorLink: "https://theoyr.us"
description: "cek size node_modules dengan cepat."
resources:
- name: "featured-image"
  src: "node-modules.webp"

tags: ["node_modules","js","node"]
categories: ["JavaScript"]

lightgallery: true
---

# Cara cepat cek size node_modules di Linux/Mac

Mungkin ada kalanya kita butuh mengecek berapa size node_modules yang terpasang di project-project kita. Jika kita seperti kebanyakan js dev, kita sering menjalankan npm install atau yarn. Nah pasti, kita mungkin memiliki banyak direktori node_modules dari proyek lama yang tidak terpakai lagi.

Si folder node_modules ini sudah gak relevan lagi, dan malah membuat hard drive kita membengkak, hampir habis, atau membuat komputer lola :angry: Tenang kawand. Berikut ini cara mengetahui dengan tepat berapa banyak direktori node_modules yang telah kita instal di komputer (Mac, Linux) serta berapa banyak space yang digunakan secara individual dan totalnya.

## Cari semua si biang kerok node_modules :angry:

Masuk ke direktori mana kita mau mencari si biang kerok ini, misal saya ada di folder ~/devel.

```
# Linux or Mac
find . -name "node_modules" -type d -prune -print | xargs du -chs
```

Ini bisa memakan waktu, tergantung pada seberapa banyak project (antah berantah) emhh maksudnya yang luar biasa :grin: yang kita miliki di komputer.

```
> find . -name "node_modules" -type d -prune -print | xargs du -chs
304M    ./react/my-project1/node_modules
539M    ./react/admin-jersipedia/node_modules
246M    ./react/pwa-js/node_modules
394M    ./react/materio-mui-react-nextjs-admin-template-free/typescript-version/node_modules
732M    ./react/tokyo-free-black-react-admin-dashboard/node_modules
458M    ./react/react-ts-ionic-pwa-capacitor/node_modules
159M    ./react/react-ts-mui-pwa-capacitor/node_modules
214M    ./loopback/api/node_modules
272M    ./django/django-react-boilerplate-master/node_modules
310M    ./django/drf-SimpleJWT-React-master/jwt-react/node_modules
179M    ./sails/hello/node_modules
84M     ./sails/api/node_modules
2.2M    ./js/basic-rabbitmq/node_modules
39M     ./js/hapi-note/node_modules
1.9M    ./js/hapi-ws/node_modules
408M    ./js/test-admin/node_modules
115M    ./js/dicoding-hapi-openmusic/node_modules
111M    ./js/notes-app-back-end/node_modules
39M     ./js/dicoding-hapi-bookshelf/node_modules
61M     ./js/tes/node_modules
32M     ./js/openmusic-app-queue-consumer/node_modules
550M    ./js/dmg-evo-web/node_modules
469M    ./js/nextjs/fe-web-nextjs/node_modules
8.0K    ./js/nextjs/node_modules
32M     ./js/notes-app-queue-consumer/node_modules
72M     ./js/express/express-ts/node_modules
480M    ./ionic/react/ion-react/node_modules
670M    ./ionic/react/web/node_modules
371M    ./ionic/react/login_auth-master/node_modules
372M    ./ionic/react/dmg-lasiap/node_modules
685M    ./ionic/angular/ui-metronic.diginetmedia.co.id.angular/demo1/node_modules
698M    ./ionic/angular/web/node_modules
104K    ./ionic/angular/android_sicantik_migrasi_goid_v2-leaflet/plugins/cordova.plugins.diagnostic/node_modules
287M    ./ionic/angular/android_sicantik_migrasi_goid_v2-leaflet/node_modules
8.0K    ./go/skeleton/node_modules
323M    ./angular/halo/node_modules
9.5G    total
 /home/devel ------------------------------------------------------------------ 27s | 22:54:52
```

Sialan ternyata 9,5 GB :grin:

## Kita bumi hanguskan si node_modules ini :angry:

Klo kita mau bener-bener putus hubungan :grin:, jalankan perintah berikut

```
# Linux or Mac
find . -name 'node_modules' -type d -prune -print -exec rm -rf '{}' \;
```

Eh, beneran gapapa nih? Emh gapapa, klo mau CLBK kan kita tinggal npm install atau yarn install aja sesuai project yg mau kita jalankan. Tapi untuk saat ini kita ga mau sama mantan dulu, matre space disk! hahaha :grin:

Semoga bermanfaat :)

> disadur dari gist 
[https://gist.github.com/theoyrus/afd7f6073ef7393a0695206a2da60afd](https://gist.github.com/theoyrus/afd7f6073ef7393a0695206a2da60afd)
