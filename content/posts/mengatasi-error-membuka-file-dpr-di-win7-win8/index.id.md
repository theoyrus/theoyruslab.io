---
weight: 5
title: "Mengatasi Error Membuka File .dpr Di Windows 7 atau Windows 8"
date: 2013-07-05T02:05:00+07:00
lastmod: 2013-07-05T02:05:00+07:00
draft: false
resources:
- name: "featured-image"
  src: "pesan-error.png"
- name: "featured-image-preview"
  src: "pesan-error.png"

tags: ["delphi"]
categories: ["Delphi"]

lightgallery: true
---

Pernahkah anda mengalami pesan error ketika anda ingin membuka file project Delphi di Windows 7 atau Windows 8?

<!--more-->

Kepraktisan dan optimalisasi penggunaan komputer itu sangat diperlukan, komputer diciptakan memang untuk memudahkan pekerjaan manusia. Tapi apabila komputer kita belum optimal dalam penggunaan, tentu akan membosankan dan menghabiskan waktu melakukan perkerjaan yang berulang-ulang.

## Problem

Pernahkah anda mengalami pesan error ketika anda ingin membuka file project Delphi di Windows 7 atau Windows 8?

Jika ya, pasti anda tidak asing dengan pesan error ini :grin:

![Tampilan error](https://3.bp.blogspot.com/-aEpbpdWGC9U/UdW8T_ONBeI/AAAAAAAAAIM/76h08oPHA_0/s471/pesan-error.png)

IDE Delphi berhasil dibuka, namun project kita tidak bisa terbuka, terpaksa kita membuka file project Delphi kita lewat menu bar atau speed bar Open, dan malah harus browse lagi file project kita, capee dee :grin:

Saya dulu juga sering mengalami hal ini, melalui tulisan acak-acakan saya ini, saya akan share pengalaman saya mengatasi error “menyebalkan” ini, hehe :). Tips ini mungkin bisa menjadi salah satu cara “mengakali” error ini, jadi ketika kita ingin membuka file project Delphi, kita cukup klik kanan file tersebut klik klik dan project terbuka :grin:

Udah dong bro cuap-cuapnya, kasi tau dong!
Sipp, caranya kita cukup pake program kecil yang namanya Open++, ga sampe 1 MB ukurannya kok. Wah, open++? Ente otak ngeres ni :stuck_out_tongue:

Haha, ya walau namanya Open++, tapi bukan berarti program ini isinya hal-hal yang berbau XXX alias beep :grin: OpenXX ini konsepnya mirip dengan “Open With” dari menu klik kanannya Explorer kita, jadi tinggal konfigurasi dikit, maka kemudahan akses akan didapatkan :)

Screenshootnya gan

![](https://3.bp.blogspot.com/-Y2l74SiDWvw/UdXBofcQLTI/AAAAAAAAAJI/WZfS2X5BLnk/s818/scr-openxx.png)

[![](https://4.bp.blogspot.com/-M5s7s5ovLrs/UdW8cNVcOfI/AAAAAAAAAI8/QktK87FVE2w/s604/yee.png)](https://4.bp.blogspot.com/-M5s7s5ovLrs/UdW8cNVcOfI/AAAAAAAAAI8/QktK87FVE2w/s604/yee.png)

Lebih praktis kan? Tinggal klik kanan file project kita, dan tinggal pilih mau dibuka pake program apa. ;)

Tutornya dong gan :)
Oke, lets do it! Hehe

## Langkah-langkah Pemasangan

[![](https://1.bp.blogspot.com/-Y2l74SiDWvw/UdXBofcQLTI/AAAAAAAAAJM/WRsWAfNFy2Y/s818/scr-openxx.png)](https://1.bp.blogspot.com/-Y2l74SiDWvw/UdXBofcQLTI/AAAAAAAAAJM/WRsWAfNFy2Y/s818/scr-openxx.png)

1.  Pertama-pertama kita perlu download program OpenXX ini, tenang ukurannya kecil ko ga sampe 1 MB, jadi irit kuota internet deh :grin:
    Link downloadnya :
    situs resmi :  [disini](https://dengdun.webs.com/en/openxx.zip)
    google drive saya : [disini](https://googledrive.com/host/0Byq38J29B_8TdVhOeVdpVGdvWXc)
    > Update [link baru disini](https://drive.google.com/open?id=0Byq38J29B_8TdVhOeVdpVGdvWXc)

1.  Kalo udah di download, tinggal extract filenya di direktori yang kamu inginkan. Misalnya saja saya extract di D:\theoyrus\bin\openxx

1.  Kalo udah, tinggal jalankan file application bernama OpenXX.exe, maka akan nongol jin yang sakti eh,,,tampilan aplikasinya, berikut penampakannya gan :


    [![](https://1.bp.blogspot.com/-u2el8goDJvA/UdW8b3MNrDI/AAAAAAAAAIw/d0pLd0elN1k/s514/tampilan.png)](https://1.bp.blogspot.com/-u2el8goDJvA/UdW8b3MNrDI/AAAAAAAAAIw/d0pLd0elN1k/s514/tampilan.png)

1.  Nah melalui window ini kita bisa main-main sama context menunya explorer, bisa nambah-nambah menu klik kanan explorer tanpa harus mengedit-edit registry. Namun sebelumnya, kita harus menginstallkan aplikasi ini di menu klik kanan, jadi ketika klik kanan explorer kita, aplikasi ini bisa tampil sebagaimana mestinya.
    Caranya?
    buka tab Install/Uninstall, lalu klik button Install…, maka akan terinstall di context menu klik kanan explorer. (kalau UAC kompi anda aktif, klik Yes aja)

    [![](https://3.bp.blogspot.com/-QIcXMfsDwAI/UdW8a8txSUI/AAAAAAAAAIc/MJqoXP0GWak/s482/installasi.png)](https://3.bp.blogspot.com/-QIcXMfsDwAI/UdW8a8txSUI/AAAAAAAAAIc/MJqoXP0GWak/s482/installasi.png)

1.  Yee,, Open++ telah terinstall di menu klik kanan explorer kita, tinggal kita menambah menu untuk membuka file project Delphi kita. Buka lagi tab Commands, tinggal klik tombol Add > Command

    [![](https://3.bp.blogspot.com/-3ZXQdbrujKA/UdW8aivDH7I/AAAAAAAAAIY/Xq3dslFU-IA/add.png)](https://3.bp.blogspot.com/-3ZXQdbrujKA/UdW8aivDH7I/AAAAAAAAAIY/Xq3dslFU-IA/s678/add.png)

    dan isi konfigurasinya seperti dibawah ini

    [![](https://2.bp.blogspot.com/-YZ83p8Nllkc/UdW8bJD7iYI/AAAAAAAAAIk/aegz05paS8Q/s475/konfig.png)](https://2.bp.blogspot.com/-YZ83p8Nllkc/UdW8bJD7iYI/AAAAAAAAAIk/aegz05paS8Q/s475/konfig.png)

    *   Title : Nama konteks menu kita < bebas >
    *   Program : letak program/command kita, kalo Delphi 7 biasanya di C:\Program Files\Borland\Delphi7\Bin\delphi32.exe
    *   Arguments : sebuah parameter yang akan dieksekusi oleh program, dan kali ini kita akan membuka file Delphi maka argumennya adalah letak/path file kita yang disimpan dalam variable bernama %FilePaths% (klik button […] untuk menyisipkannya.
        Misal file project berada di D:\sim\sim.drp > Jadi ketika dijalankan perintahnya menjadi C:\Program Files\Borland\Delphi7\Bin\delphi32.exe “D:\sim\sim.drp”
    *   Working directory : letak direktori kerja kita. Kalo kita lihat dari shortcutnya Delphi di desktop atau di Start Menu, maka working direktorinya adalah Start in : "C:\Program Files\Borland\Delphi7\Bin” atau yang lainnya, namun hal ini bersifat optional, maka disini tidak perlu diisi tidak apa-apa.
    *   Icon : misalnya kita ingin menambahkan icon di menu klik kanan buatan Open++ maka bisa kita isikan dengan membrowse/ klik button […] dan pilih icon program kita. Dalam tutorial ini file icon berada di file exe Delphi32.exe, ketika anda browse, dan pilih exe dari aplikasi yg bericon maka akan keluar icon-icon aplikasi tersebut, silahkan pilih saja.
    *   Associate with : maksudnya aktifkan menu ini ketika kita melakukan klik kanan terhadap sebuah objek, misal file atau folder. Di Open++ menyediakan banyak opsi, mulai dari multiple sampai single, maksudnya ketika misal kita menandai beberapa file project, maka kita menu buatan kita akan ditampilkan.
        Jadi kalo kita memilih opsi Multiple Files maka menu akan ditampilkan ketika kita menandai beberapa file project, dan jika Multiple Files or Folders maka menu akan ditampilkan ketika kita menandai beberapa file ataupun folder.
    *   File types : untuk mengaktifkan menu ini hanya untuk file-file berekstensi yang diisikan, pada tutorial ini diisi dengan “*.dpr” . Maka menu ini hanya aktif/berlaku saat kita mengklik kanan semua file (* berarti semua) berekstensi .dpr saja.
9.  Klik apply dan atau ok untuk menyimpan menu kita, maka saat anda mengklik kanan file berekstensi .dpr, akan muncul submenu Buka Pake Delphi 7 di menu Open++

## Hasil

  [![](https://4.bp.blogspot.com/-M5s7s5ovLrs/UdW8cNVcOfI/AAAAAAAAAI8/QktK87FVE2w/s604/yee.png)](https://4.bp.blogspot.com/-M5s7s5ovLrs/UdW8cNVcOfI/AAAAAAAAAI8/QktK87FVE2w/s604/yee.png)

Wah akhirnya selesai juga sharing ini, semoga bermanfaat bagi anda :)
