---
title: "Tentang Saya"
date: 2022-09-12T14:42:06+07:00
draft: false

lightgallery: true

math:
  enable: true
---
### Hanya perkenalan singkat

Hai, terima kasih telah mengunjungi blog sederhana ini. Jika anda menyempatkan mengakses laman ini, sepertinya anda ingin tahu tentang saya.

Izinkan saya memperkenalkan diri, namaku Suryo Prasetyo Wibowo, anda bisa memanggil saya Suryo. Saya punya nama panggilan di internet, theoyrus (nama tersebut merupakan kebalikan huruf dari nama panggilan saya, oyrus == suryo) :grin:. 

Tetap ingin terhubung dengan saya? bisa hubungi melalui : 

| Social Media | Link |
| ------------ | ---- |
| Facebook | [https://www.facebook.com/theoyrus](https://www.facebook.com/theoyrus) |
| Twitter | [https://twitter.com/theoyrus](https://twitter.com/theoyrus), bukan akun sosial media primer sih, jadi maaf jika telat merespon :smile: |
| Linked In | [http://id.linkedin.com/in/theoyrus](http://id.linkedin.com/in/theoyrus), jalin relasi bisa dengan ini juga |
| Email | the[dot]oyrus[at]gmail[dot]com, email ini juga email aktif, jika ada yang bisa disampaikan juga bisa melalui email tersebut |

### About versi absurd

Jika ingin melihat laman saya versi sebelumnya ketika masih menggunakan platform blogspost, bisa buka di [laman ini](../about-absurd) :see_no_evil:
