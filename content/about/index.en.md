---
title: "About Me"
date: 2022-09-12T14:42:06+07:00
draft: false

lightgallery: true

math:
  enable: true
---
### Just a short introduction

Hi, thank you for coming to my simple blog. If you access this page, it means you want to know me.

Let me introduce myself, My name is Suryo Prasetyo Wibowo, you can call me Suryo. I have nickname, named theoyrus (that name is the reverse word of my nickname oyrus == suryo) :grin:. 

Stay connect with me via : 

| Social Media | Link |
| ------------ | ---- |
| Facebook | [https://www.facebook.com/theoyrus](https://www.facebook.com/theoyrus) |
| Twitter | [https://twitter.com/theoyrus](https://twitter.com/theoyrus), but it's not primary social media, sorry for late response :smile: |
| Linked In | [http://id.linkedin.com/in/theoyrus](http://id.linkedin.com/in/theoyrus), build professional relationship |
| Email | the[dot]oyrus[at]gmail[dot]com, is active!, If there is something that can be conveyed, it can also be via email |
